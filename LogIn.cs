﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace Account
{
    public partial class LogIn : Form
    {
        public LogIn()
        {
            InitializeComponent();
            this.textBoxLogInPassword.AutoSize = false;
            this.textBoxLogInPassword.Size = new Size(this.textBoxLogInPassword.Size.Width, 27);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataBase db = new DataBase();
            String username = textBoxLogInUsername.Text;
            String password = textBoxLogInPassword.Text;

            DataTable table = new DataTable();
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            MySqlCommand command = new MySqlCommand("SELECT * FROM `users` WHERE `username`=@usn and `password`=@pass", db.GetConnection());

            command.Parameters.Add("@usn", MySqlDbType.VarChar).Value = username;
            command.Parameters.Add("@pass", MySqlDbType.VarChar).Value = password;

            adapter.SelectCommand = command;
            adapter.Fill(table);

            if(table.Rows.Count>0)
            {
               
                Welcome welcome = new Welcome();
                welcome.Show();
            }
            else
            {
                MessageBox.Show("This account does not exist!");
            }
        }

        private void textBoxLogInUsername_Enter(object sender, EventArgs e)
        {
            string username = textBoxLogInUsername.Text;
            if(username.Equals("username"))
            {
                textBoxLogInUsername.Text = "";
            }
        }

        private void textBoxLogInPassword_Enter(object sender, EventArgs e)
        {
            string password = textBoxLogInPassword.Text;
            if (password.Equals("password"))
            {
                textBoxLogInPassword.Text = "";
            }
        }

        private void textBoxLogInUsername_Leave(object sender, EventArgs e)
        {
            String username = textBoxLogInUsername.Text;
            if (username.Equals("username") || username.Equals(""))
            {
                textBoxLogInUsername.Text = "username";
            }
        }

        private void textBoxLogInPassword_Leave(object sender, EventArgs e)
        {
            String password = textBoxLogInPassword.Text;
            if (password.Equals("password") || password.Equals(""))
            {
                textBoxLogInPassword.Text = "password";
            }
        }
    }
}
