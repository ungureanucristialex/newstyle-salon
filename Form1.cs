﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Account
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
            this.textBoxPassword.AutoSize = false;
            this.textBoxPassword.Size = new Size(this.textBoxPassword.Size.Width, 30);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            LogIn log = new LogIn();
            log.Show();
        }

        private void textBoxUsername_Enter(object sender, EventArgs e)
        {
            string username = textBoxUsername.Text;
            if (username.Equals("username"))
            {
                textBoxUsername.Text = "";
            }
        }

        private void textBoxPassword_Enter(object sender, EventArgs e)
        {
            string password = textBoxPassword.Text;
            if (password.Equals("password"))
            {
                textBoxPassword.Text = "";
            }
        }

        private void textBoxEmail_Enter(object sender, EventArgs e)
        {
            string email = textBoxEmail.Text;
            if (email.Equals("email"))
            {
                textBoxEmail.Text = "";
            }
        }

        private void textBoxUsername_Leave(object sender, EventArgs e)
        {
            String username = textBoxUsername.Text;
            if(username.Equals("username")||username.Equals(""))
            {
                textBoxUsername.Text = "username";
            }
        }

        private void textBoxEmail_Leave(object sender, EventArgs e)
        {
            String email = textBoxEmail.Text;
            if (email.Equals("email") || email.Equals(""))
            {
                textBoxEmail.Text = "email";
            }
        }

        private void textBoxPassword_Leave(object sender, EventArgs e)
        {
            String password = textBoxPassword.Text;
            if (password.Equals("password") || password.Equals(""))
            {
                textBoxPassword.Text = "password";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataBase db = new DataBase();
            MySqlCommand command = new MySqlCommand("INSERT INTO `users`( `username`, `password`, `email`) VALUES (@usr,@pass,@email)", db.GetConnection());
            command.Parameters.Add("@usr", MySqlDbType.VarChar).Value = textBoxUsername.Text;
            command.Parameters.Add("@pass", MySqlDbType.VarChar).Value = textBoxPassword.Text;
            command.Parameters.Add("@email", MySqlDbType.VarChar).Value = textBoxEmail.Text;

            db.openConnection();
            if (checkUsername())
            {
                MessageBox.Show("This Username already exist");
            }
            else
            {


                if (command.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show(" Account Created");
                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
            db.closeConnection();
        }
        public Boolean checkUsername()
        {
            DataBase db = new DataBase();
            String username = textBoxUsername.Text;

            DataTable table = new DataTable();
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            MySqlCommand command = new MySqlCommand("SELECT * FROM `users` WHERE `username`=@usn", db.GetConnection());

            command.Parameters.Add("@usn", MySqlDbType.VarChar).Value = username;

            adapter.SelectCommand = command;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
          
        }
    }
}
